import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CourseUpdateRequest} from '../../../interfaces/CourseUpdateRequest';
import {CourseService} from '../../../services/course.service';
import {Course} from '../../../interfaces/Course';
import {Role} from '../../../interfaces/role';
import {Classroom} from '../../../interfaces/Classroom';


@Component({
  selector: 'app-update-course',
  templateUrl: './update-course.component.html',
  styleUrls: ['./update-course.component.css']
})
export class UpdateCourseComponent implements OnInit {
  courseToUpdate: CourseUpdateRequest = {
    courseId: null,
    classroom: {classroomId: null, capacity: null, facilities: '', course: null},
    subject: {subjectId: null, requirements: null, course: null},
    period: '', time: null
  };

  courseId: number;
  private sub: any;
  private courses: Course[];
  classrooms: Classroom[] = [];
  selectedClassroom: Classroom = {classroomId: null, capacity: null, facilities: '', course: null};
  classroomId: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private courseService: CourseService) {
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.courseId = params.id;
    });
    this.courseService.getCourseToEdit(this.courseId).subscribe(data => {
      this.courseToUpdate = data;
      this.courseId = this.courseToUpdate.courseId;
    });
    this.courseService.getAll().subscribe(data => this.courses = data);
  }

  openCoursePage(): void {
    this.router.navigate(['course-list']).then(r => console.log('Course list....'));
  }

  update(): void {
    this.courseService.update(this.courseToUpdate.courseId, this.courseToUpdate).subscribe(data => this.openCoursePage());
  }

}
