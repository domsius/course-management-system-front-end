import { Component, OnInit } from '@angular/core';
import {Course} from '../../../interfaces/Course';
import {Router} from '@angular/router';

import {UserService} from '../../../services/user.service';
import {CourseService} from '../../../services/course.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  courses: Course[];
  editCourseId: number;

  constructor(
    private courseService: CourseService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.courseService.getAll().subscribe(data => this.courses = data);
  }

  delete(): void {
    if (this.editCourseId){
      this.courseService.delete(this.editCourseId).subscribe(data => this.getData());
    }
  }

  openRegisterCoursePage(): void {
    this.router.navigate(['/create-course']).then(r => console.log('Edit course....'));
  }

  openEditCoursePage(): void {
    if (this.editCourseId) {
      this.router.navigate(['/update-course', this.editCourseId]).then(r => console.log('Register a new course....'));
    }
  }
  openCourseDetailsPage(): void {
    if (this.editCourseId) {
      this.router.navigate(['/course-details', this.editCourseId]).then(r => console.log('Register a new course....'));
    }
  }

  onToggle(courseId: number): void {
    this.editCourseId = courseId;
  }
}

