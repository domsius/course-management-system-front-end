import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CourseService} from '../../../services/course.service';
import {CourseCreateRequest} from '../../../interfaces/CourseCreateRequest';

@Component({
  selector: 'app-create-course',
  templateUrl: './create-course.component.html',
  styleUrls: ['./create-course.component.css']
})
export class CreateCourseComponent implements OnInit {
  course: CourseCreateRequest = {
    courseId: null,
    classroom: {classroomId: null, capacity: null, facilities: '', course: null},
    subject: {subjectId: null, requirements: null, course: null},
    period: '', time: null
  };

  constructor(
    private courseService: CourseService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  openCourseListPage(pageName: string): void {
    this.router.navigate([pageName]).then(r => console.log('Opening course list...'));
  }

  addCourse(): void {
    this.courseService.addCourse(this.course).subscribe(data => this.openCourseListPage('/course-list'));
  }
}
