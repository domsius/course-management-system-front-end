import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Course} from '../../../interfaces/Course';
import {CourseService} from '../../../services/course.service';
import {User} from '../../../interfaces/user';

@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent implements OnInit {
  courseId: number;
  course: Course;
  user: User;

  constructor(private route: ActivatedRoute, private courseService: CourseService) {
  }

  ngOnInit(): void {
    this.courseId = this.route.snapshot.params.id;

    this.courseService.getCourseToEdit(this.courseId).subscribe(data => {
      this.course = data;
    });
  }

}

