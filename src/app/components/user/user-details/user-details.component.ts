import { Component, OnInit } from '@angular/core';
import {User} from '../../../interfaces/user';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  id: number;
  user: User;
  constructor(private route: ActivatedRoute, private userService: UserService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;

    this.user = new User();
    this.userService.getUserToEdit(this.id).subscribe( data => {
      this.user = data;
    });
  }

}
