import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';
import {UserService} from '../../../services/user.service';
import {UserCreateRequest} from '../../../interfaces/UserCreateRequest';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  user: UserCreateRequest = {username: '', password: '', firstName: '', lastName: ''};

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  openUserListPage(pageName: string): void {
    this.router.navigate([pageName]).then(r => console.log('Opening user list...'));
  }

  addUser(): void {
    this.userService.addUser(this.user).subscribe(data => this.openUserListPage('/users'));
  }
}
