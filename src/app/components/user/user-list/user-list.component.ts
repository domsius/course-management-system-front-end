import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../../../interfaces/user';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: User[];
  editUserId: number;

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.userService.getAll().subscribe(data => this.users = data);
  }

  delete(): void {
    if (this.editUserId){
      this.userService.delete(this.editUserId).subscribe(data => this.getData());
    }
  }

  openRegisterUserPage(): void {
    this.router.navigate(['/create-user']).then(r => console.log('Edit user....'));
  }

  openEditUserPage(): void {
    if (this.editUserId) {
      this.router.navigate(['/update-user', this.editUserId]).then(r => console.log('Register a new user....'));
    }
  }
  openUserDetailsPage(): void {
    if (this.editUserId) {
      this.router.navigate(['/user-details', this.editUserId]);
    }
  }

  onToggle(id: number): void {
    this.editUserId = id;
  }
}

