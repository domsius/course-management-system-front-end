import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {UserService} from '../../../services/user.service';
import {Role} from '../../../interfaces/role';
import {RoleService} from '../../../services/role.service';
import {UserUpdateRequest} from '../../../interfaces/UserUpdateRequest';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  userToUpdate: UserUpdateRequest = {id: null, firstName: '', lastName: '', username: '', password: '', role: null};
  roles: Role[] = [];
  selectedRole: Role = {id: null, name: '', nameEnum: ''};
  userId: number;
  roleId: number;
  private sub: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private roleService: RoleService) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.userId = params.id;
    });
    this.userService.getUserToEdit(this.userId).subscribe(data => {
      this.userToUpdate = data;
      this.roleId = this.userToUpdate.role.id;
    });
    this.roleService.getAll().subscribe(data => this.roles = data);
  }

  openUserPage(): void {
    this.router.navigate(['users/']).then(r => console.log('User list....'));
  }

  update(): void {
    this.userService.update(this.userToUpdate.id, this.userToUpdate).subscribe(data => this.openUserPage());
  }

  logRole(idAsString: string): void {
    this.selectedRole = this.roles.find(x => x.id === Number(idAsString));
    this.userToUpdate.role = this.selectedRole;
    console.log('Selected role: ' + this.selectedRole.name);
    console.log('User to update role: ' + this.userToUpdate.role.name);
  }
}
