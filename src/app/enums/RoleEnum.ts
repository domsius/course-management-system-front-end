export enum RoleEnum {
  ROLE_STUDENT,
  ROLE_TEACHER,
  ROLE_ADMIN
}
