import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {UserListComponent} from './components/user/user-list/user-list.component';
import {CreateUserComponent} from './components/user/create-user/create-user.component';
import {UpdateUserComponent} from './components/user/update-user/update-user.component';
import {UserDetailsComponent} from './components/user/user-details/user-details.component';
import {HomeComponent} from './components/home/home.component';
import {CreateCourseComponent} from './components/course/create-course/create-course.component';
import {CourseListComponent} from './components/course/course-list/course-list.component';
import {UpdateCourseComponent} from './components/course/update-course/update-course.component';
import {CourseDetailsComponent} from './components/course/course-details/course-details.component';



const routes: Routes = [
  {path: '', redirectTo: 'users', pathMatch: 'full'},
  {path: 'users', component: UserListComponent},
  {path: 'create-user', component: CreateUserComponent},
  {path: 'update-user/:id', component: UpdateUserComponent},
  {path: 'user-details/:id', component: UserDetailsComponent},
  {path: 'home', component: HomeComponent},
  {path: 'create-course', component: CreateCourseComponent},
  {path: 'course-list', component: CourseListComponent},
  {path: 'update-course/:id', component: UpdateCourseComponent},
  {path: 'course-details/:id', component: CourseDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
