import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { UserListComponent } from './components/user/user-list/user-list.component';
import { CreateUserComponent } from './components/user/create-user/create-user.component';
import { UpdateUserComponent } from './components/user/update-user/update-user.component';
import {UserDetailsComponent} from './components/user/user-details/user-details.component';
import { HomeComponent } from './components/home/home.component';
import { CreateCourseComponent } from './components/course/create-course/create-course.component';
import {CourseListComponent} from './components/course/course-list/course-list.component';
import { UpdateCourseComponent } from './components/course/update-course/update-course.component';
import { CourseDetailsComponent } from './components/course/course-details/course-details.component';




@NgModule({
  declarations: [
    AppComponent,

    UserListComponent,
    CreateUserComponent,
    UpdateUserComponent,
    UserDetailsComponent,
    HomeComponent,
    CreateCourseComponent,
    CourseListComponent,
    UpdateCourseComponent,
    CourseDetailsComponent



  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
