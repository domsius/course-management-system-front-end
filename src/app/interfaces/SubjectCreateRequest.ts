import {Course} from './Course';

export interface SubjectCreateRequest {
  subjectId: number;
  requirements: number;
  course: Course;
}
