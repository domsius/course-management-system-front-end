import {Course} from './Course';

export interface ClassroomUpdateRequest {
  classroomId: number;
  capacity: number;
  facilities: string;
  course: Course;
}
