import {Classroom} from './Classroom';
import {Subject} from './Subject';

export interface Course {
  courseId: number;
  classroom: Classroom;
  subject: Subject;
  period: string;
  time: Date;
}
