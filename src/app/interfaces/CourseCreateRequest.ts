import {Classroom} from './Classroom';
import {Subject} from './Subject';

export interface CourseCreateRequest{
  courseId: number;
  classroom: Classroom;
  subject: Subject;
  period: string;
  time: Date;
}
