import {Course} from './Course';

export interface ClassroomCreateRequest {
  classroomId: number;
  capacity: number;
  facilities: string;
  course: Course;
}
