import {Course} from './Course';

export interface SubjectUpdateRequest {
  subjectId: number;
  requirements: number;
  course: Course;
}
