import {Classroom} from './Classroom';
import {Subject} from './Subject';

export interface CourseUpdateRequest{
  courseId: number;
  classroom: Classroom;
  subject: Subject;
  period: string;
  time: Date;
}
