import {Role} from './role';

export interface UserUpdateRequest{
  id: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  role: Role;
}
