import {Course} from './Course';

export interface Classroom {
 classroomId: number;
 capacity: number;
 facilities: string;
 course: Course;
}
