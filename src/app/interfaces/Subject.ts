import {Course} from './Course';

export interface Subject {
  subjectId: number;
  requirements: number;
  course: Course;
}
