import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../interfaces/user';
import {environment} from '../../environments/environment';
import {UserUpdateRequest} from '../interfaces/UserUpdateRequest';
import {UserCreateRequest} from '../interfaces/UserCreateRequest';



@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseUrl = environment.apiUrl + 'api/v1/user';

  constructor(private http: HttpClient) { }

  getAll(): Observable<User[]>{
    return this.http.get<User[]>(this.baseUrl);
  }

  getUserToEdit(id: number): Observable<UserUpdateRequest> {
    return this.http.get<UserUpdateRequest>(`${this.baseUrl}/${id}`);
  }

  addUser(userCreateRequest: UserCreateRequest): Observable<any> {
    return this.http.post(`${this.baseUrl}/add`, userCreateRequest);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  update(id: number, user: UserUpdateRequest): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}`, user);
  }
}
