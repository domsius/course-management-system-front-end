import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Role} from '../interfaces/role';

import {User} from '../interfaces/user';
import {Course} from '../interfaces/Course';
import {Classroom} from '../interfaces/Classroom';

@Injectable({
  providedIn: 'root'
})
export class ClassroomService {
  private baseUrl = environment.apiUrl + 'api/v1/classroom';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Classroom[]> {
    return this.http.get<Classroom[]>(this.baseUrl);
  }

  getCoursesByClassroom(classroom: Course): Observable<User[]>{
    return this.http.get<User[]>(`${this.baseUrl}/${'by?classroom=' + classroom}`);
  }
}
