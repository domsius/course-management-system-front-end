import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {environment} from '../../environments/environment';
import {CourseCreateRequest} from '../interfaces/CourseCreateRequest';
import {Course} from '../interfaces/Course';
import {CourseUpdateRequest} from '../interfaces/CourseUpdateRequest';
import {Subject} from '../interfaces/Subject';
import {UserUpdateRequest} from '../interfaces/UserUpdateRequest';



@Injectable({
  providedIn: 'root'
})
export class CourseService {

  private baseUrl = environment.apiUrl + 'api/v1/course';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Course[]> {
    return this.http.get<Course[]>(this.baseUrl);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  getCourseById(id: number): Observable<Course>{
    return this.http.get<Course>(`${this.baseUrl}/${id}`);
  }

  addCourse(courseCreateRequest: CourseCreateRequest): Observable<any> {
    return this.http.post(`${this.baseUrl}/add`, courseCreateRequest);
  }
  getCourseToEdit(id: number): Observable<CourseUpdateRequest> {
    return this.http.get<CourseUpdateRequest>(`${this.baseUrl}/${id}`);
  }

  update(id: number, course: CourseUpdateRequest): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}`, course);
  }
}
