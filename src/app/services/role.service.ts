import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Role} from '../interfaces/role';
import {Observable} from 'rxjs';
import {User} from '../interfaces/user';
import {RoleEnum} from '../enums/RoleEnum';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private baseUrl = environment.apiUrl + 'api/v1/role';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Role[]> {
    return this.http.get<Role[]>(this.baseUrl);
  }

  getUsersByRole(role: RoleEnum): Observable<User[]>{
    return this.http.get<User[]>(`${this.baseUrl}/${'by?role=' + role}`);
  }
}
